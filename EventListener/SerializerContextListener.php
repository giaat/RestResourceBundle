<?php

namespace GI\RestResourceBundle\EventListener;

use GI\RestResourceBundle\Serializer\SerializerContextGroupsBuilderInterface;
use FOS\RestBundle\View\ViewHandler;
use GI\RestResourceBundle\Exception\RuntimeException;
use GI\RestResourceBundle\Util\RequestAttributesExtractor;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

/**
 * Class SerializerContextListener
 *
 * @package RestBundle\EventListener
 */
class SerializerContextListener
{
    /**
     * @var ViewHandler
     */
    private $viewHandler;

    /**
     * @var SerializerContextGroupsBuilderInterface
     */
    private $serializerContextGroupsBuilder;

    /**
     * SerializeContextListener constructor.
     * @param ViewHandler $viewHandler
     * @param SerializerContextGroupsBuilderInterface $serializerContextGroupsBuilder
     */
    public function __construct(
        ViewHandler $viewHandler,
        SerializerContextGroupsBuilderInterface $serializerContextGroupsBuilder
    ) {
        $this->viewHandler = $viewHandler;
        $this->serializerContextGroupsBuilder = $serializerContextGroupsBuilder;
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $data = $event->getControllerResult();
        $request = $event->getRequest();
        try {
            $attributes = RequestAttributesExtractor::extractAttributes($request);
        } catch (RuntimeException $e) {
            return;
        }

        if ($data instanceof FormInterface) {
            return;
        }

        $groups = $this->serializerContextGroupsBuilder->createFromRequest($request, $data, true, $attributes);

        $this->viewHandler->setExclusionStrategyGroups($groups);
    }
}