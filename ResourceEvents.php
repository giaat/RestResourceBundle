<?php

namespace GI\RestResourceBundle;

/**
 * Class ResourceEvents
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle
 */
final class ResourceEvents
{
    const PRE_VALIDATE = 'pre_validate';
    const POST_VALIDATE = 'post_validate';

    const PRE_CREATE = 'pre_create';
    const POST_CREATE = 'post_create';

    const PRE_UPDATE = 'pre_update';
    const POST_UPDATE = 'post_update';

    const PRE_DELETE = 'pre_delete';
    const POST_DELETE = 'post_delete';

    /**
     * @param string $manager
     * @param string $eventName
     *
     * @return string
     */
    public static function build($resourceName, $eventName)
    {
        return sprintf('%s.%s', $resourceName, $eventName);
    }
}
