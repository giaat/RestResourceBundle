<?php

namespace GI\RestResourceBundle\Serializer;

use GI\RestResourceBundle\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Builds the context used by the Symfony Serializer.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
interface SerializerContextGroupsBuilderInterface
{
    /**
     * Creates a serialization context from a Request.
     *
     * @param Request    $request
     * @param string     $subject
     * @param bool       $normalization
     * @param array|null $extractedAttributes
     *
     * @throws RuntimeException
     *
     * @return array
     */
    public function createFromRequest(
        Request $request,
        $subject,
        bool $normalization,
        array $extractedAttributes = null
    ): array;
}
