<?php

namespace GI\RestResourceBundle\Serializer;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Exception\UnsupportedFormatException;
use JMS\Serializer\Serializer;
use GI\RestResourceBundle\Util\RequestAttributesExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use JMS\Serializer\Exception\Exception as JMSSerializerException;

/**
 * Class RequestDeserializerBuilder
 * @package RestBundle\Serializer
 */
class RequestDeserializerBuilder
{

    /**
     * @var SerializerContextGroupsBuilderInterface
     */
    private $serializerContextGroupsBuilder;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * RequestDeserializerBuilder constructor.
     *
     * @param SerializerContextGroupsBuilderInterface $serializerContextGroupsBuilder
     * @param Serializer $serializer
     */
    public function __construct(
        SerializerContextGroupsBuilderInterface $serializerContextGroupsBuilder,
        Serializer $serializer
    ) {
        $this->serializerContextGroupsBuilder = $serializerContextGroupsBuilder;
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     * @param string $className
     * @param $object
     *
     * @return array|\JMS\Serializer\scalar|mixed|object
     */
    public function createObjectFromRequest(Request $request, string $className, $object = null)
    {
        $attributes = RequestAttributesExtractor::extractAttributes($request);
        $groups = $this->serializerContextGroupsBuilder->createFromRequest($request, $object, false, $attributes);

        $context = DeserializationContext::create();
        $context->setGroups($groups);
        if ($object !== null) {
            $context->setAttribute('target', $object);
        }

        try {
            $data = $this->serializer->deserialize(
                $request->getContent(),
                $className,
                $request->getContentType(),
                $context
            );
        } catch (UnsupportedFormatException $e) {
            throw new UnsupportedMediaTypeHttpException($e->getMessage(), $e);
        } catch (JMSSerializerException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        }

        return $data;
    }
}