<?php

namespace GI\RestResourceBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\DirectoryResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Finder\Finder;

/**
 * Class RestExtension
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\DependencyInjection
 */
class GIRestResourceExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $availableBundles = $container->getParameter('kernel.bundles');
        $bundles = [];
        foreach ($config['bundles'] as $bundle) {
            if (!array_key_exists($bundle, $availableBundles)) {
                throw new \RuntimeException(sprintf("Bundle %s not found.", $bundle));
            }
            $bundles[] = $availableBundles[$bundle];
        }

        $this->registerLoaders($container, $bundles);
    }

    private function registerLoaders(ContainerBuilder $container, array $bundles)
    {
        $classes = [];
        $resourceClassDirectories = [];
        $finder = new Finder();

        foreach ($bundles as $bundle) {
            $reflectionClass = new \ReflectionClass($bundle);
            $bundleDirectory = dirname($reflectionClass->getFileName());

            if (file_exists($entityDirectory = $bundleDirectory . '/Entity')) {
                $resourceClassDirectories[] = $entityDirectory;
                $container->addResource(new DirectoryResource($entityDirectory, '/\.php$/'));
                $finder->files()->in($entityDirectory)->name('*.php');
                foreach ($finder as $file) {
                    $classes[] = $reflectionClass->getNamespaceName() . '\\Entity\\' . $file->getBasename('.php');
                }
            }

            if (file_exists($documentDirectory = $bundleDirectory . '/Document')) {
                $container->addResource(new DirectoryResource($documentDirectory, '/\.php$/'));
                $resourceClassDirectories[] = $documentDirectory;

                $finder->files()->in($bundleDirectory)->name('*.php');
                foreach ($finder as $file) {
                    $classes[] = $reflectionClass->getNamespaceName() . '\\Document\\' . $file->getBasename('.php');
                }
            }
        }


        $container->setParameter('rest.resource_class_directories', $resourceClassDirectories);
        $container->getDefinition('rest.metadata.rest_resource.collection_factory.annotation')->addArgument($classes);
        //$container->getDefinition('api_platform.metadata.extractor.yaml')->addArgument($yamlResources);
    }
}
