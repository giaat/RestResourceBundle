<?php

namespace GI\RestResourceBundle\Manager;

use Doctrine\Common\Persistence\ObjectRepository;

interface ResourceManagerInterface extends ObjectRepository
{
    /**
     * @return string
     */
    public function getShortName();

    /**
     * {@inheritdoc}
     */
    public function create();

    /**
     * {@inheritdoc}
     */
    public function save($entity, $andFlush = true, $force = false);

    /**
     * {@inheritdoc}
     */
    public function remove($entity, $andFlush = true);

    /**
     * Returns the related Object Repository.
     * @param string $class
     *
     * @return ObjectRepository
     */
    public function getRepository($class = null);

    /**
     * @param $object
     *
     * @throws \InvalidArgumentException
     */
    public function checkObject($object);


    /**
     * @param name $name
     */
    public function __call($name, $args);
}