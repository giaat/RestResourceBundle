<?php

namespace GI\RestResourceBundle\Manager;

use Pagerfanta\Pagerfanta;


/**
 * Interface PageableManagerInterface
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Manager
 */
interface ResourcePageableManagerInterface
{
    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $sort
     *
     * @return Pagerfanta
     */
    public function getPager(array $criteria = []);
}