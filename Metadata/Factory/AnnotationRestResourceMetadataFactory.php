<?php

namespace GI\RestResourceBundle\Metadata\Factory;

use Doctrine\Common\Annotations\Reader;
use GI\RestResourceBundle\Annotation\RestResource;
use GI\RestResourceBundle\Exception\ResourceClassNotFoundException;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;

/**
 * Class AnnotationRestResourceMetadataFactory
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Metadata\Factory
 */
class AnnotationRestResourceMetadataFactory
{
    /**
     * @var Reader
     */
    private $reader;

    /**
     * AnnotationRestResourceCollectionFactory constructor.
     *
     * @param Reader $reader
     */
    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @param string $resourceClass
     *
     * @return RestResourceMetadata
     *
     * @throws ResourceClassNotFoundException
     */
    public function create(string $resourceClass): RestResourceMetadata
    {
        try {
            $reflectionClass = new \ReflectionClass($resourceClass);
        } catch (\ReflectionException $reflectionException) {
            throw new ResourceClassNotFoundException(sprintf('Resource "%s" not found.', $resourceClass));
        }

        /** @var RestResource $resourceAnnotation */
        $resourceAnnotation = $this->reader->getClassAnnotation($reflectionClass, RestResource::class);
        if ($resourceAnnotation === null) {
            throw new \Exception(sprintf(
                'Resource annotation for "%s" not found.',
                $resourceClass
            ));
        }

        return $this->createMetadata($resourceAnnotation, $resourceClass);
    }

    /**
     * @param RestResource $annotation
     *
     * @return RestResourceMetadata
     */
    public function createMetadata(RestResource $annotation, string $resourceClass): RestResourceMetadata
    {
        if ($annotation->name === null) {
            $annotation->name = substr($resourceClass, strrpos($resourceClass, '\\') + 1);
        }

        if ($annotation->collectionOperations === null) {
            $annotation->collectionOperations = [
                'get'   => ['method' => 'GET'],
                'post'  => ['method' => 'POST']
            ];
        }

        if ($annotation->itemOperations === null) {
            $annotation->itemOperations = [
                'get'       => ['method' => 'GET'],
                'put'       => ['method' => 'PUT'],
                'patch'     => ['method' => 'PATCH'],
                'delete'    => ['method' => 'DELETE']
            ];
        }

        return new RestResourceMetadata(
            $annotation->name,
            $annotation->manager,
            $annotation->collectionOperations,
            $annotation->itemOperations,
            $annotation->attributes
        );
    }
}
