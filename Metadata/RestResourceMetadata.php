<?php

namespace GI\RestResourceBundle\Metadata;

/**
 * Class ResourceMetadata
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Metadata
 */
class RestResourceMetadata
{

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string
     */
    private $manager;

    /**
     * @var array|null
     */
    private $collectionOperations;

    /**
     * @var array|null
     */
    private $itemOperations;

    /**
     * @var array|null
     */
    public $attributes;

    /**
     * RestResourceMetadata constructor.
     *
     * @param null|string $name
     * @param string      $manager
     * @param array|null  $collectionOperations
     * @param array|null  $itemOperations
     * @param array|null  $attributes
     */
    public function __construct(
        string $name,
        string $manager = null,
        array $collectionOperations = null,
        array $itemOperations = null,
        array $attributes = null
    ) {
        $this->name = $name;
        $this->manager = $manager;
        $this->collectionOperations = $collectionOperations;
        $this->itemOperations = $itemOperations;
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param string $manager
     *
     * @return RestResourceMetadata
     */
    public function setManager(string $manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     *
     * @return RestResourceMetadata
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getCollectionOperations()
    {
        return $this->collectionOperations;
    }

    /**
     * @param array|null $collectionOperations
     *
     * @return RestResourceMetadata
     */
    public function setCollectionOperations($collectionOperations)
    {
        $this->collectionOperations = $collectionOperations;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getItemOperations()
    {
        return $this->itemOperations;
    }

    /**
     * @param array|null $itemOperations
     *
     * @return RestResourceMetadata
     */
    public function setItemOperations($itemOperations)
    {
        $this->itemOperations = $itemOperations;

        return $this;
    }

    /**
     * Gets a collection operation attribute, optionally fallback to a resource attribute.
     *
     * @param string $operationName
     * @param string $key
     * @param mixed  $defaultValue
     * @param bool   $resourceFallback
     *
     * @return mixed
     */
    public function getCollectionOperationAttribute(
        string $operationName,
        string $key,
        $defaultValue = null,
        bool $resourceFallback = false
    ) {
        return $this->getOperationAttribute(
            $this->collectionOperations,
            $operationName,
            $key,
            $defaultValue,
            $resourceFallback
        );
    }

    /**
     * Gets an item operation attribute, optionally fallback to a resource attribute.
     *
     * @param string $operationName
     * @param string $key
     * @param mixed  $defaultValue
     * @param bool   $resourceFallback
     *
     * @return mixed
     */
    public function getItemOperationAttribute(
        string $operationName,
        string $key,
        $defaultValue = null,
        bool $resourceFallback = false
    ) {
        return $this->getOperationAttribute(
            $this->itemOperations,
            $operationName,
            $key,
            $defaultValue,
            $resourceFallback
        );
    }

    /**
     * Gets an operation attribute, optionally fallback to a resource attribute.
     *
     * @param array|null $operations
     * @param string     $operationName
     * @param string     $key
     * @param mixed      $defaultValue
     * @param bool       $resourceFallback
     *
     * @return mixed
     */
    private function getOperationAttribute(
        array $operations = null,
        string $operationName,
        string $key,
        $defaultValue = null,
        bool $resourceFallback = false
    ) {
        if (isset($operations[$operationName][$key])) {
            return $operations[$operationName][$key];
        }

        if ($resourceFallback && isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }

        return $defaultValue;
    }

    /**
     * @return array|null
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array|null $attributes
     *
     * @return RestResourceMetadata
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }
}
