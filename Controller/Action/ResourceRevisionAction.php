<?php

namespace GI\RestResourceBundle\Controller\Action;

use Gedmo\Loggable\Entity\LogEntry;
use GI\RestResourceBundle\Controller\AbstractRestController;
use GI\RestResourceBundle\Loader\ResourceMetadata;
use GI\RestResourceBundle\Manager\ResourceManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class RevisionListAction
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Controller\Action
 */
class ResourceRevisionAction extends AbstractRestController
{
    /**
     * @param Request                  $request
     * @param ResourceManagerInterface $manager
     * @param                          $id
     *
     * @return array
     */
    public function listAction(Request $request, ResourceManagerInterface $manager, $id)
    {
        $entity = $this->get($request, $manager, $id);
        $repo = $manager->getObjectManager()->getRepository('Gedmo\Loggable\Entity\LogEntry');
        $entries = $repo->getLogEntries($entity);
        $logs = [];
        foreach ($entries as $entry) {
            $logs[] = [
                'id' => $entry->getId(),
                'action' => $entry->getAction(),
                'logged_at' => $entry->getLoggedAt(),
                'version' => $entry->getVersion(),
                'username' => $entry->getUsername()
            ];
        }

        return $logs;
    }

    /**
     * @param Request                  $request
     * @param ResourceManagerInterface $manager
     * @param                          $id
     * @param                          $revision
     *
     * @return \FOS\RestBundle\View\View|object
     */
    public function getAction(Request $request, ResourceManagerInterface $manager, $id, $revision)
    {
        $entity = $this->get($request, $manager, $id);

        if (!is_numeric($revision)) {
            throw new BadRequestHttpException('Revision is not numeric');
        }

        $repo = $manager->getObjectManager()->getRepository('Gedmo\Loggable\Entity\LogEntry');
        $repo->revert($entity, $revision);

        return $entity;
    }
}
