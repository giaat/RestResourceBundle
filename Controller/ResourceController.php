<?php

namespace GI\RestResourceBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use GI\RestResourceBundle\Manager\ResourceManagerInterface;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResourceController
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Controller
 */
class ResourceController extends AbstractRestController
{

    /**
     * @param Request $request
     *
     * @return array
     */
    public function cgetAction(Request $request, ResourceManagerInterface $manager)
    {
        return $this->search($request, $manager);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return array|View
     */
    public function getAction(Request $request, ResourceManagerInterface $manager, $id)
    {
        return $this->get($request, $manager, $id);
    }

    /**
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     *
     * @param Request $request
     * @param         $id
     *
     * @return array|View
     */
    public function postAction(
        Request $request,
        ResourceManagerInterface $manager,
        RestResourceMetadata $resourceMetadata
    ) {
        return $this->post($request, $manager, $resourceMetadata);
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_ACCEPTED)
     *
     * @param Request $request
     * @param         $id
     *
     * @return array|View
     */
    public function putAction(
        Request $request,
        ResourceManagerInterface $manager,
        RestResourceMetadata $resourceMetadata,
        $id
    ) {
        return $this->put($request, $manager, $resourceMetadata, $id);
    }

    public function deleteAction(
        Request $request,
        ResourceManagerInterface $manager,
        $id
    ) {
        return $this->delete($request, $manager, $id);
    }
}
