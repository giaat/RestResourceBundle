<?php

namespace GI\RestResourceBundle\Controller;

use GI\RestResourceBundle\Event\ResourceEvent;
use GI\RestResourceBundle\Exception\ValidationException;
use GI\RestResourceBundle\Manager\ResourceManagerInterface;
use GI\RestResourceBundle\Manager\ResourcePageableManagerInterface;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\ResourceEvents;
use GI\RestResourceBundle\Security\Domain\ResourcePermission;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use GI\RestResourceBundle\Security\Domain\ResourceIdentity;
use GI\RestResourceBundle\Serializer\RequestDeserializerBuilder;
use GI\RestResourceBundle\Util\RequestAttributesExtractor;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractRestController
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Controller
 */
abstract class AbstractRestController implements ClassResourceInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var RequestDeserializerBuilder
     */
    protected $requestDeserializerBuilder;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * AbstractRestController constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TokenStorageInterface $tokenStorage
     * @param EventDispatcherInterface $dispatcher
     * @param RequestDeserializerBuilder $requestDeserializerBuilder
     * @param ValidatorInterface $validator
     */
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage,
        EventDispatcherInterface $dispatcher,
        RequestDeserializerBuilder $requestDeserializerBuilder,
        ValidatorInterface $validator
    ) {

        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->dispatcher = $dispatcher;
        $this->requestDeserializerBuilder = $requestDeserializerBuilder;
        $this->validator = $validator;
    }

    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied object.
     *
     * @param mixed $attributes The attributes
     * @param mixed $object     The object
     *
     * @return bool
     *
     * @throws \LogicException
     */
    protected function isGranted($attributes, $object = null)
    {
        return $this->authorizationChecker->isGranted($attributes, $object);
    }

    /**
     * Throws an exception unless the attributes are granted against the current authentication token and optionally
     * supplied object.
     *
     * @param mixed  $attributes The attributes
     * @param mixed  $object     The object
     * @param string $message    The message passed to the exception
     *
     * @throws AccessDeniedException
     */
    protected function denyAccessUnlessGranted($attributes, $object = null, $message = 'Access Denied.')
    {
        if (!$this->isGranted($attributes, $object)) {
            throw new AccessDeniedException($message);
        }
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     */
    public function getUser()
    {

        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }

    /**
     * @param       $eventName
     * @param Event $event
     *
     * @return Event
     */
    protected function dispatch($eventName, Event $event)
    {
        return $this->dispatcher->dispatch($eventName, $event);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    protected function search(Request $request, ResourceManagerInterface $manager)
    {
        $this->isGrantedForList($request, $manager);
        if ($manager instanceof ResourcePageableManagerInterface) {
            $criterias = [];
            $criterias['_sort_field'] = $request->query->get('order');

            $criterias['_query'] = $request->query->all();

            return $manager->getPager($criterias);
        }

        return $manager->findAll();
    }

    /**
     * @param Request                  $request
     * @param ResourceManagerInterface $manager
     */
    protected function isGrantedForList(Request $request, ResourceManagerInterface $manager)
    {
        $identity = new ResourceIdentity(
            $manager->getClassName(),
            ResourceIdentity::IDENTIFIER_COLLECTION,
            $request->attributes->get('_rest_collection_operation_name')
        );

        $this->denyAccessUnlessGranted(ResourcePermission::LIST, $identity);
    }

    /**
     * @param $id
     *
     * @return object|View
     */
    protected function get(Request $request, ResourceManagerInterface $manager, $id)
    {
        $object = is_object($id) ? $id : $manager->find($id);
        if (!$object) {
            throw new NotFoundHttpException();
        }

        $manager->checkObject($object);
        $this->isGrantedForView($request, $object);

        return $object;
    }

    /**
     * @param Request                  $request
     * @param ResourceManagerInterface $manager
     */
    protected function isGrantedForView(Request $request, $object)
    {
        $identity = new ResourceIdentity(
            $object,
            $object->getId(),
            $request->attributes->get('_rest_item_operation_name')
        );
        $this->denyAccessUnlessGranted(ResourcePermission::VIEW, $identity);
    }

    /**
     * @param Request $request
     * @param         $object
     *
     * @return mixed
     */
    protected function post(
        Request $request,
        ResourceManagerInterface $manager,
        RestResourceMetadata $resourceMetadata,
        $object = null,
        $isFile = false
    ) {
        if ($object) {
            $manager->checkObject($object);
        }

        $this->isGrantedForCreate($request, $manager);

        $data = $this
                ->requestDeserializerBuilder
                ->createObjectFromRequest($request, $manager->getClassName(), $object);

        $this->validateObject($request, $manager, $resourceMetadata, $data);

        $event = new ResourceEvent($data, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_CREATE),
            $event
        );
        $manager->save($data);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_CREATE),
            $event
        );

        return $data;
    }

    /**
     * @param Request                  $request
     * @param ResourceManagerInterface $manager
     */
    protected function isGrantedForCreate(Request $request, ResourceManagerInterface $manager)
    {
        $identity = new ResourceIdentity(
            $manager->getClassName(),
            ResourceIdentity::IDENTIFIER_NEW,
            $request->attributes->get('_rest_collection_operation_name')
        );
        $this->denyAccessUnlessGranted(ResourcePermission::CREATE, $identity);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return View
     */
    protected function put(Request $request, ResourceManagerInterface $manager, RestResourceMetadata $resourceMetadata, $id)
    {
        $object = is_object($id) ? $id : $manager->find($id);
        if (!$object) {
            throw new NotFoundHttpException();
        }

        $manager->checkObject($object);
        $this->isGrantedForEdit($request, $object);

        $data = $this
            ->requestDeserializerBuilder
            ->createObjectFromRequest($request, $manager->getClassName(), $object);

        $this->validateObject($request, $manager, $resourceMetadata, $data);

        $event = new ResourceEvent($data, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_UPDATE),
            $event
        );
        $manager->save($data);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_UPDATE),
            $event
        );

        return $data;
    }

    /**
     * @param Request $request
     * @param         $object
     */
    protected function isGrantedForEdit(Request $request, $object)
    {
        $identity = new ResourceIdentity(
            $object,
            $object->getId(),
            $request->attributes->get('_rest_item_operation_name')
        );
        $this->denyAccessUnlessGranted(ResourcePermission::EDIT, $identity);
    }


    /**
     * @param Request                  $request
     * @param ResourceManagerInterface $manager
     * @param                          $id
     *
     * @return null
     */
    protected function delete(Request $request, ResourceManagerInterface $manager, $id)
    {
        $object = is_object($id) ? $id : $manager->find($id);
        if (!$object) {
            throw new NotFoundHttpException();
        }

        $manager->checkObject($object);
        $this->isGrantedForEdit($request, $object);

        $event = new ResourceEvent($object, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_DELETE),
            $event
        );

        $manager->remove($object);

        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_DELETE),
            $event
        );

        return null;
    }

    /**
     * @param Request $request
     * @param         $object
     */
    protected function isGrantedForDelete(Request $request, $object)
    {
        $identity = new ResourceIdentity(
            $object,
            $object->getId(),
            $request->attributes->get('_rest_item_operation_name')
        );
        $this->denyAccessUnlessGranted(ResourcePermission::DELETE, $identity);
    }

    /**
     * @param Request $request
     * @param RestResourceMetadata $resourceMetadata
     * @param $data
     *
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    protected function validateObject(
        Request $request,
        ResourceManagerInterface $manager,
        RestResourceMetadata $resourceMetadata,
        $data
    ) {
        $event = new ResourceEvent($data, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_VALIDATE),
            $event
        );

        $attributes = RequestAttributesExtractor::extractAttributes($request);

        if (isset($attributes['collection_operation_name'])) {
            $validationGroups = $resourceMetadata->getCollectionOperationAttribute($attributes['collection_operation_name'], 'validation_groups');
        } else {
            $validationGroups = $resourceMetadata->getItemOperationAttribute($attributes['item_operation_name'], 'validation_groups');
        }

        if (!$validationGroups) {
            // Fallback to the resource
            $validationGroups = $resourceMetadata->getAttributes()['validation_groups'] ?? null;
        }

        if (is_callable($validationGroups)) {
            $validationGroups = call_user_func_array($validationGroups, [$data]);
        }

        $violations = $this->validator->validate($data, null, $validationGroups);
        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }

        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_VALIDATE),
            $event
        );
    }

    /**
     * @param $method
     *
     * @return MethodNotAllowedHttpException
     */
    protected function createMethodNotAllowedException($method)
    {
        return new MethodNotAllowedHttpException(['GET', 'DELETE'], $method . ' method is not allowed.');
    }
}
